class ResultPrinter():

    def __init__(self, store):
        self.count = -1
        self.store = store

    def perform(self):
        for garaza in self.store.garaze:
            self.count += 1
            print("Garaza {}".format(self.count))
            print("velicina - {}, lokacija - {}, automatska vrata - {}".format(garaza.size, garaza.location, garaza.automatic))
            print("Sadrzi predmete:")
            for item in garaza.items:
                print("{}, vrijednost: {}".format(item.name, item.value))
            print(" ")