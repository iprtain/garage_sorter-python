from garaza import *
from item import *

class Questions():

    def __init__(self, store):
        self.store = store

    def about_garaza(self, garaza):
        print("molim vas unesite velicinu garaze {}".format(garaza))
        size = input()
        print("molim vas unesite lokaciju garaze {}".format(garaza))
        location = input()
        print("jesu li vrata automatska (DA/NE) {}".format(garaza))
        automatic = input()
        self.store.add_garaza(Garaza(size=size, location=location, automatic=automatic))



    def about_predmet(self, predmet):
        print("molim vas unesite redni broj garaze u koju spada {}.predmet(bez tocke)".format(predmet + 1))
        garaza = int(input()) 
        print("unesite naziv predmeta")
        name = input()
        print("unesite vrijednost predmeta")
        value = input()
        self.store.garaze[garaza].add_item(Item(name, value))